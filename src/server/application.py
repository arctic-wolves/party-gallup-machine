"""
Following the application setup pattern concept as in https://github.com/svieira/Budget-Manager
"""
from flask import Flask

from api.party import party_route
from api.vote import vote_route
from api.voter import voter_route
from models.shared import db


def create_app(config_path=None, name=None):
    """Creates a flask application instance and loads its config from config_path
    :param config_path: A string representing either a file path or an import path
    :returns: An instance of `flask.Flask`."""

    app = Flask(name or __name__)

    app.register_blueprint(party_route)
    app.register_blueprint(voter_route)
    app.register_blueprint(vote_route)

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///pwp.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app_title = "Party Group Machine"

    db.init_app(app)
    return app
