from flask import Blueprint

from models import Session
from models.voter import Voter

voter_route = Blueprint('voter_route', __name__, template_folder='templates')


@voter_route.route('/voters')
def get_voters():
    session = Session()
    for instance in session.query(Voter).order_by(Voter.id):
        print(instance.id, instance.age, instance.sex)
    # TODO: make return json
    return "make this return json"
