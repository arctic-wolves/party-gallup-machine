from flask import Blueprint

from models import Session
from models.party import Party

party_route = Blueprint('party_route', __name__, template_folder='templates')


@party_route.route('/parties')
def get_parties():
    session = Session()
    for instance in session.query(Party).order_by(Party.id):
        print(instance.id, instance.name, instance.abbreviation)
    # TODO: make return json
    return "make this return json"
