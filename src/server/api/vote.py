from flask import Blueprint

from models import Session
from models.vote import Vote

vote_route = Blueprint('vote_route', __name__, template_folder='templates')


@vote_route.route('/votes')
def get_votes():
    session = Session()
    for instance in session.query(Vote).order_by(Vote.id):
        print(instance.id, instance.party_id, instance.voter_id)
    # TODO: make return json
    return "make this return json"
