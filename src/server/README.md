# Party Gallup Machine RESTful API server

## Installation instructions
### Quick start for testing (by using the populated pwp.db)
1. Clone the project master branch with 'git clone https://gitlab.com/arctic-wolves/party-gallup-machine/'
2. Navigate to project /src/server directory
3. Install python 3.6 and create and activate virtual environment for the project
4. Install required modules by executing command 'pip install -r requirements.txt' on this directory
5. Run the app by runnning the run_server.py file on python3 eg. 'python3 run_server.py'
6. You can see the app running by navigating to localhost:5000 on your browser
7. Database test data can be printed on console by performing GET method to routes
    1. /parties
    2. /voters
    3. /votes

### Creating database and tables
You can create a database with empty tables
1. cd src/server
2. python3 manage.py create_db

### Populating the database
You can populate the database with data declared in utils.test_data. Warning: this drops existing database!
1. cd src/server
2. python3 manage.py seed


## Specs
SQlite version used: '2.6.0'

