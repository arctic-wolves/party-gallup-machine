"""
Following the application setup pattern concept as in https://github.com/svieira/Budget-Manager
"""
import os

from flask_script import Manager

from application import create_app
from models import Session
from models.shared import db
# import test data for db seeding
from utils.test_data import *

app = create_app()
command_runner = Manager(app)


@command_runner.command
def create_db():
    """Creates the necessary tables in the database"""

    print("Initializing tables ...")
    db.metadata.create_all(db.engine)
    print("Done!")


@command_runner.command
def seed():
    """Populates the database with test data"""

    """Drop old database for seeding"""
    print("Deleting existing database ...")
    os.remove("pwp.db")
    print("Database deleted!")

    """Create tables"""
    print("Initializing tables ...")
    create_db()
    print("Tables initialized!")

    """Insert test data to tables from utils.test_data"""
    print("Seeding the database ...")
    session = Session()
    session.bulk_save_objects(party_objects)
    session.bulk_save_objects(voter_objects)
    session.bulk_save_objects(vote_objects)
    session.commit()
    print("Done!")


if __name__ == "__main__":
    command_runner.run()
