#!/usr/bin/env python

from os import path

from application import create_app

here = path.dirname(path.abspath(__file__))

application = create_app('BUDGET_CONFIG_PATH')


if __name__ == '__main__':
    application.run(debug=True)
