from models.shared import db

class Party(db.Model):
    """
    DB model for party
    """
    # surrogate party id
    id = db.Column(db.Integer, primary_key=True)
    # party name
    name = db.Column(db.String(64), nullable=False, unique=True)
    # party abbreviation eg. Vihr.
    abbreviation = db.Column(db.String(8), nullable=False, unique=True)
    # party main theme color in hex eg. #0042ad
    color = db.Column(db.String(32), nullable=False)

    # associations
    # TODO: proper assiciation
    # votes = db.relationship("Vote", back_populates="party")

    # representation
    def __repr__(self):
        return "<Party(name='%s', abbreviation='%s', color='%s')>" % (
                            self.name, self.abbreviation, self.color)
