from models.shared import db
from utils.enum import Sex


class Voter(db.Model):
    """
    DB model for voter
    """
    # voter's surrogate id
    id = db.Column(db.Integer, primary_key=True)
    # voter's age eg. 30
    age = db.Column(db.Integer, nullable=False)
    # voter's sex presented in enumeration
    sex = db.Column(db.Enum(Sex))

    # associations
    # TODO: proper associations
    # vote = db.relationship("Vote", back_populates="voter", uselist=False)
