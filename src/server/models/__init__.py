"""
https://docs.sqlalchemy.org/en/latest/orm/session_basics.html
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# an Engine, which the Session will use for connection
# resources
engine = create_engine('sqlite:///pwp.db')

# create a configured "Session" class
Session = sessionmaker(bind=engine)
