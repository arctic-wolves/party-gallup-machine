from models.shared import db


class Vote(db.Model):
    """
    DB model for vote
    """
    # vote id
    id = db.Column(db.Integer, primary_key=True)
    # party id (foreign key)
    party_id = db.Column(db.Integer, db.ForeignKey("party.id", ondelete="SET NULL"))
    # voter id (foreign key)
    voter_id = db.Column(db.Integer, db.ForeignKey("voter.id", ondelete="SET NULL"))

    # associations
    # TODO: proper associations
    # party = db.relationship("Party", back_populates="votes", uselist=False)
    # voter = db.relationship("Voter", back_populates="vote", uselist=False)
