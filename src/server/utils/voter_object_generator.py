import random

""" Creates a object list of eg. format:
Voter(
 sex=Sex.male,
 age=35
)"""

voter_amount = 100

enums = ["Sex.male", "Sex.female", "Sex.other"]
generated_voter_objects = open("generated_voter_objects", "w")
for _ in range(voter_amount):
    generated_voter_objects.write(
        "Voter(\n   sex={},\n   age={}\n),".format(random.choice(enums), random.randint(18, 90))
    )
