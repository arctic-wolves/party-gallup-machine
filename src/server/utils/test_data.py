from models.party import Party
from models.vote import Vote
from models.voter import Voter
from utils.enum import Sex

party_objects = [
    Party(
        name="Kansallinen Kokoomus",
        abbreviation="Kok.",
        color="#6dcff6"
    ),
    Party(
        name="Suomen Keskusta",
        abbreviation="Kesk.",
        color="#01954b"
    ),
    Party(
        name="Suomen Sosialidemokraattinen Puolue",
        abbreviation="SDP",
        color="#e11931"
    ),
    Party(
        name="Sininen tulevaisuus",
        abbreviation="Sin.",
        color="#031f73"
    ),
    Party(
        name="Perussuomalaiset",
        abbreviation="PS",
        color="#ffde55"
    ),
    Party(
        name="Vihreä liitto",
        abbreviation="Vihr.",
        color="#61bf1a"
    ),
    Party(
        name="Vasemmistoliitto",
        abbreviation="Vas,",
        color="#bf1e24"
    ),
    Party(
        name="Suomen ruotsalainen kansanpuolue",
        abbreviation="RKP",
        color="#ffdd93"
    ),
    Party(
        name="Suomen Kristillisdemokraatit",
        abbreviation="KD",
        color="#18359b"
    ),
    Party(
        name="Seitsemän tähden liike",
        abbreviation="TL",
        color="#239dac"
    )
]

voter_objects = [
    Voter(
        sex=Sex.other,
        age=39
    ), Voter(
        sex=Sex.other,
        age=27
    ), Voter(
        sex=Sex.other,
        age=63
    ), Voter(
        sex=Sex.male,
        age=85
    ), Voter(
        sex=Sex.male,
        age=37
    ), Voter(
        sex=Sex.male,
        age=76
    ), Voter(
        sex=Sex.other,
        age=46
    ), Voter(
        sex=Sex.other,
        age=43
    ), Voter(
        sex=Sex.male,
        age=70
    ), Voter(
        sex=Sex.other,
        age=88
    ), Voter(
        sex=Sex.female,
        age=64
    ), Voter(
        sex=Sex.other,
        age=45
    ), Voter(
        sex=Sex.male,
        age=43
    ), Voter(
        sex=Sex.female,
        age=57
    ), Voter(
        sex=Sex.female,
        age=40
    ), Voter(
        sex=Sex.female,
        age=33
    ), Voter(
        sex=Sex.female,
        age=42
    ), Voter(
        sex=Sex.other,
        age=76
    ), Voter(
        sex=Sex.other,
        age=54
    ), Voter(
        sex=Sex.other,
        age=43
    ), Voter(
        sex=Sex.female,
        age=23
    ), Voter(
        sex=Sex.male,
        age=81
    ), Voter(
        sex=Sex.female,
        age=41
    ), Voter(
        sex=Sex.female,
        age=45
    ), Voter(
        sex=Sex.male,
        age=49
    ), Voter(
        sex=Sex.female,
        age=18
    ), Voter(
        sex=Sex.other,
        age=26
    ), Voter(
        sex=Sex.female,
        age=45
    ), Voter(
        sex=Sex.male,
        age=52
    ), Voter(
        sex=Sex.other,
        age=32
    ), Voter(
        sex=Sex.other,
        age=78
    ), Voter(
        sex=Sex.male,
        age=88
    ), Voter(
        sex=Sex.female,
        age=40
    ), Voter(
        sex=Sex.other,
        age=77
    ), Voter(
        sex=Sex.other,
        age=26
    ), Voter(
        sex=Sex.other,
        age=64
    ), Voter(
        sex=Sex.other,
        age=36
    ), Voter(
        sex=Sex.other,
        age=37
    ), Voter(
        sex=Sex.female,
        age=25
    ), Voter(
        sex=Sex.male,
        age=23
    ), Voter(
        sex=Sex.female,
        age=74
    ), Voter(
        sex=Sex.male,
        age=19
    ), Voter(
        sex=Sex.female,
        age=88
    ), Voter(
        sex=Sex.other,
        age=67
    ), Voter(
        sex=Sex.female,
        age=76
    ), Voter(
        sex=Sex.male,
        age=26
    ), Voter(
        sex=Sex.male,
        age=68
    ), Voter(
        sex=Sex.female,
        age=62
    ), Voter(
        sex=Sex.male,
        age=30
    ), Voter(
        sex=Sex.other,
        age=77
    ), Voter(
        sex=Sex.male,
        age=22
    ), Voter(
        sex=Sex.female,
        age=55
    ), Voter(
        sex=Sex.female,
        age=89
    ), Voter(
        sex=Sex.male,
        age=61
    ), Voter(
        sex=Sex.male,
        age=40
    ), Voter(
        sex=Sex.female,
        age=45
    ), Voter(
        sex=Sex.female,
        age=74
    ), Voter(
        sex=Sex.male,
        age=85
    ), Voter(
        sex=Sex.male,
        age=70
    ), Voter(
        sex=Sex.male,
        age=42
    ), Voter(
        sex=Sex.other,
        age=18
    ), Voter(
        sex=Sex.other,
        age=31
    ), Voter(
        sex=Sex.other,
        age=32
    ), Voter(
        sex=Sex.male,
        age=28
    ), Voter(
        sex=Sex.other,
        age=33
    ), Voter(
        sex=Sex.male,
        age=62
    ), Voter(
        sex=Sex.male,
        age=89
    ), Voter(
        sex=Sex.other,
        age=57
    ), Voter(
        sex=Sex.other,
        age=69
    ), Voter(
        sex=Sex.female,
        age=83
    ), Voter(
        sex=Sex.male,
        age=22
    ), Voter(
        sex=Sex.male,
        age=22
    ), Voter(
        sex=Sex.other,
        age=29
    ), Voter(
        sex=Sex.male,
        age=70
    ), Voter(
        sex=Sex.female,
        age=58
    ), Voter(
        sex=Sex.male,
        age=47
    ), Voter(
        sex=Sex.male,
        age=87
    ), Voter(
        sex=Sex.other,
        age=46
    ), Voter(
        sex=Sex.male,
        age=69
    ), Voter(
        sex=Sex.female,
        age=58
    ), Voter(
        sex=Sex.male,
        age=58
    ), Voter(
        sex=Sex.female,
        age=45
    ), Voter(
        sex=Sex.other,
        age=64
    ), Voter(
        sex=Sex.other,
        age=31
    ), Voter(
        sex=Sex.other,
        age=49
    ), Voter(
        sex=Sex.other,
        age=79
    ), Voter(
        sex=Sex.male,
        age=20
    ), Voter(
        sex=Sex.other,
        age=49
    ), Voter(
        sex=Sex.female,
        age=41
    ), Voter(
        sex=Sex.female,
        age=58
    ), Voter(
        sex=Sex.female,
        age=69
    ), Voter(
        sex=Sex.other,
        age=79
    ), Voter(
        sex=Sex.male,
        age=29
    ), Voter(
        sex=Sex.female,
        age=41
    ), Voter(
        sex=Sex.male,
        age=89
    ), Voter(
        sex=Sex.female,
        age=83
    ), Voter(
        sex=Sex.other,
        age=40
    ), Voter(
        sex=Sex.male,
        age=68
    ), Voter(
        sex=Sex.male,
        age=66
    ), Voter(
        sex=Sex.male,
        age=56
    )
]

vote_objects = [
    Vote(
        party_id=10,
        voter_id=1
    ), Vote(
        party_id=7,
        voter_id=2
    ), Vote(
        party_id=1,
        voter_id=3
    ), Vote(
        party_id=9,
        voter_id=4
    ), Vote(
        party_id=5,
        voter_id=5
    ), Vote(
        party_id=10,
        voter_id=6
    ), Vote(
        party_id=5,
        voter_id=7
    ), Vote(
        party_id=6,
        voter_id=8
    ), Vote(
        party_id=2,
        voter_id=9
    ), Vote(
        party_id=1,
        voter_id=10
    ), Vote(
        party_id=2,
        voter_id=11
    ), Vote(
        party_id=3,
        voter_id=12
    ), Vote(
        party_id=9,
        voter_id=13
    ), Vote(
        party_id=10,
        voter_id=14
    ), Vote(
        party_id=6,
        voter_id=15
    ), Vote(
        party_id=10,
        voter_id=16
    ), Vote(
        party_id=4,
        voter_id=17
    ), Vote(
        party_id=10,
        voter_id=18
    ), Vote(
        party_id=10,
        voter_id=19
    ), Vote(
        party_id=8,
        voter_id=20
    ), Vote(
        party_id=6,
        voter_id=21
    ), Vote(
        party_id=2,
        voter_id=22
    ), Vote(
        party_id=3,
        voter_id=23
    ), Vote(
        party_id=10,
        voter_id=24
    ), Vote(
        party_id=5,
        voter_id=25
    ), Vote(
        party_id=7,
        voter_id=26
    ), Vote(
        party_id=10,
        voter_id=27
    ), Vote(
        party_id=9,
        voter_id=28
    ), Vote(
        party_id=2,
        voter_id=29
    ), Vote(
        party_id=1,
        voter_id=30
    ), Vote(
        party_id=4,
        voter_id=31
    ), Vote(
        party_id=5,
        voter_id=32
    ), Vote(
        party_id=1,
        voter_id=33
    ), Vote(
        party_id=6,
        voter_id=34
    ), Vote(
        party_id=2,
        voter_id=35
    ), Vote(
        party_id=5,
        voter_id=36
    ), Vote(
        party_id=2,
        voter_id=37
    ), Vote(
        party_id=5,
        voter_id=38
    ), Vote(
        party_id=5,
        voter_id=39
    ), Vote(
        party_id=4,
        voter_id=40
    ), Vote(
        party_id=8,
        voter_id=41
    ), Vote(
        party_id=6,
        voter_id=42
    ), Vote(
        party_id=2,
        voter_id=43
    ), Vote(
        party_id=5,
        voter_id=44
    ), Vote(
        party_id=9,
        voter_id=45
    ), Vote(
        party_id=7,
        voter_id=46
    ), Vote(
        party_id=2,
        voter_id=47
    ), Vote(
        party_id=5,
        voter_id=48
    ), Vote(
        party_id=8,
        voter_id=49
    ), Vote(
        party_id=1,
        voter_id=50
    ), Vote(
        party_id=7,
        voter_id=51
    ), Vote(
        party_id=1,
        voter_id=52
    ), Vote(
        party_id=5,
        voter_id=53
    ), Vote(
        party_id=5,
        voter_id=54
    ), Vote(
        party_id=4,
        voter_id=55
    ), Vote(
        party_id=2,
        voter_id=56
    ), Vote(
        party_id=3,
        voter_id=57
    ), Vote(
        party_id=8,
        voter_id=58
    ), Vote(
        party_id=1,
        voter_id=59
    ), Vote(
        party_id=6,
        voter_id=60
    ), Vote(
        party_id=5,
        voter_id=61
    ), Vote(
        party_id=9,
        voter_id=62
    ), Vote(
        party_id=4,
        voter_id=63
    ), Vote(
        party_id=10,
        voter_id=64
    ), Vote(
        party_id=6,
        voter_id=65
    ), Vote(
        party_id=10,
        voter_id=66
    ), Vote(
        party_id=4,
        voter_id=67
    ), Vote(
        party_id=2,
        voter_id=68
    ), Vote(
        party_id=4,
        voter_id=69
    ), Vote(
        party_id=8,
        voter_id=70
    ), Vote(
        party_id=1,
        voter_id=71
    ), Vote(
        party_id=7,
        voter_id=72
    ), Vote(
        party_id=8,
        voter_id=73
    ), Vote(
        party_id=3,
        voter_id=74
    ), Vote(
        party_id=1,
        voter_id=75
    ), Vote(
        party_id=1,
        voter_id=76
    ), Vote(
        party_id=4,
        voter_id=77
    ), Vote(
        party_id=7,
        voter_id=78
    ), Vote(
        party_id=10,
        voter_id=79
    ), Vote(
        party_id=4,
        voter_id=80
    ), Vote(
        party_id=6,
        voter_id=81
    ), Vote(
        party_id=3,
        voter_id=82
    ), Vote(
        party_id=1,
        voter_id=83
    ), Vote(
        party_id=9,
        voter_id=84
    ), Vote(
        party_id=6,
        voter_id=85
    ), Vote(
        party_id=3,
        voter_id=86
    ), Vote(
        party_id=1,
        voter_id=87
    ), Vote(
        party_id=2,
        voter_id=88
    ), Vote(
        party_id=3,
        voter_id=89
    ), Vote(
        party_id=7,
        voter_id=90
    ), Vote(
        party_id=4,
        voter_id=91
    ), Vote(
        party_id=3,
        voter_id=92
    ), Vote(
        party_id=9,
        voter_id=93
    ), Vote(
        party_id=9,
        voter_id=94
    ), Vote(
        party_id=9,
        voter_id=95
    ), Vote(
        party_id=9,
        voter_id=96
    ), Vote(
        party_id=4,
        voter_id=97
    ), Vote(
        party_id=6,
        voter_id=98
    ), Vote(
        party_id=2,
        voter_id=99
    ), Vote(
        party_id=2,
        voter_id=100
    )
]
