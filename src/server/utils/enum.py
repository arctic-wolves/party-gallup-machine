import enum


class Sex(enum.Enum):
    """
    Enumeration for sexes
    """
    male = 1
    female = 2
    # used when the user doesn't want to submit their sex information
    other = 3
