import random

""" Creates a object list of eg. format:
Vote(
    party_id=1,
    voter_id=1,
)"""

vote_amount = 100

generated_vote_objects = open("generated_vote_objects", "w")
for i in range(vote_amount):
    generated_vote_objects.write(
        "Vote(\n   party_id={},\n   voter_id={}\n),".format(random.randint(1, 10), i+1)
    )
