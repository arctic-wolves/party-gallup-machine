# Project Team Meeting Log

## Meeting #0 16.1. 
### Target 
- Prepare information to complete DL 0
- Discuss project plan and idea
- Discuss high-level implementation method
- Set up a repository for project
### Results  
- Decided to build a party gallup machine with features such as voting and results viewing
- DL 0 registration completed
- GitLab repository and project team created  
- Decided to use node.js on implementation  


## Meeting #1 23.1. 
### Target 
- User requirements for the project
- Talk about UI visualisation

### Results  
- Defined user and business requirements for the project

#### User requirements
- user can vote for one party
- user cant change decision
- user can view party voting results
- user can view total votes submitted to parties
- user can submit anonymous information about themselves
- user can authenticate as admin

- admin can post new party
- admin can edit parties
- admin can remove parties
- admin can view parties (voting targets)
- admin can view party voting statistics

#### Business logic
- prevent multiple votes from same device
- calculate percentages
- authenticate admin users

## Meeting #2 30.1. 
### Target 
- Plan UI layout

### Results  
- Decided to place voting selection and personal information form in layout to the same page
- Previewed non-functional prototype of the main voting feature
- Decided to fallback to the final deadline option because not enough time was to deliver before Sunday 5th deadline (dealine 1#).
- Confirmed that in our client-server architecture, the client is a web UI


## Meeting #3 06.02. 
### Target 
- Plan workload for DL3

### Results  
- DB design: Jari
- DB implementation: Juho
- DB testing: Timo
